const express = require('express');
const route = require('./routes.js');


const app = express();


app.use('/', route);

const PORT = 3000;

app.listen(PORT, (err)=>{
    if(err) {
        console.log(err)
    } else {
        console.log(`server listening on port ${PORT}`)
    }
})
